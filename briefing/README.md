Hi there,

Your mission, should you choose to accept it, will be to develop a little CRUD app using Angular 6
Please start by creating create a new Angular project inside the folder.

Description :

A single, list view page (list_view screen), featuring the following functionalities:

- retrieves and displays profiles list (no ordering , no pagination)
- profiles can be deleted by clicking "delete" : if so, a confirmation band drops down (confirm_delete screen)
- a profile picture can be added to / updated on each profile : clicking the avatar opens up the file browser dialog
- each profile can be flagged/unflagged as favorite using the heart icon
- clicking on the "add new" button (top right) opens up a modal form: submitting valid data through the form creates a new profile
- clicking edit on a profile item opens up the form with pre-populated fields. Submitting the form will execute a partial update on the object (PATCH)
- a green message band appears after each successful create/update/delete action (action_ok screen)

About the REST API :

- root URL with DRF Browser : http://35.195.157.128:8080/api/v1/
- if needed, here's the access to the django admin : http://35.195.157.128:8080/admin/ test@test.com:test12345
- no auth / full JSON
- only two business objects : Country and Profile
- countries are just for listing inside the form
- profile actions : list, retrieve, create, update, delete, toggle favorite and picture upload
- the "is_favorite" and "picture" fields have their own dedicated actions and cannot be part of create / update actions
- please keep in mind that each API url must end with a slash "/"

Good to know :

- you're free to use any JS/TS/CSS/SVG library you'll find relevant, but please keep it as simple as possible :)
- Please try to find a middle ground between "I'm in hurry so let's get into full quick & dirty mode" and "the 10 lines I'm going to write will be the most perfect and elegant the world has ever seen"
- 50% done logic / 50% done design will be more appreciated thant 90% done logic / 10% done design
- if you've got some time left, a few effects/animations will be much appreciated :)
- please commit your code BEFORE the deadline

Thanks for your time, and good luck !
